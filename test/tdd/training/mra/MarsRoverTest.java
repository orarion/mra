package tdd.training.mra;

import static org.junit.Assert.*;

import org.junit.Test;

import java.util.*;

public class MarsRoverTest {

	@Test
	public void testImplementazione() throws MarsRoverException {
		List<String>  planetObstacles  = new  ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover mR=new MarsRover(10, 10, planetObstacles);
		String[][] pianeta= new String[10][10];
		for (int i=9;i>=0;i--) {
			for (int j=0;j<10;j++) {
				pianeta[i][j]="("+(9-i)+","+(j)+")   ";
			}
		}
		pianeta[4][7]="(4,7) X ";
		pianeta[2][3]="(2,3) X ";
		assertEquals(pianeta[4][7],mR.getPianeta(4,7));
		assertEquals(pianeta[2][3],mR.getPianeta(2,3));
	}
	
	@Test
	public void testOstacolo() throws MarsRoverException {
		List<String>  planetObstacles  = new  ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover mR=new MarsRover(10, 10, planetObstacles);
		String[][] pianeta= new String[10][10];
		for (int i=9;i>=0;i--) {
			for (int j=0;j<10;j++) {
				pianeta[i][j]="("+(9-i)+","+(j)+")   ";
			}
		}
		pianeta[4][7]="(4,7) X ";
		pianeta[2][3]="(2,3) X ";
		assertTrue(mR.planetContainsObstacleAt(4, 7));
		assertTrue(mR.planetContainsObstacleAt(2, 3));
	}
	
	@Test
	public void testAtterraggio() throws MarsRoverException {
		List<String>  planetObstacles  = new  ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover mR=new MarsRover(10, 10, planetObstacles);
		String[][] pianeta= new String[10][10];
		for (int i=9;i>=0;i--) {
			for (int j=0;j<10;j++) {
				pianeta[i][j]="("+(9-i)+","+(j)+")   ";
			}
		}
		pianeta[4][7]="(4,7) X ";
		pianeta[2][3]="(2,3) X ";
		assertEquals("(0,0,N)",mR.executeCommand(""));
	}
	
	@Test
	public void testGiro_A_Destra() throws MarsRoverException {
		List<String>  planetObstacles  = new  ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover mR=new MarsRover(10, 10, planetObstacles);
		String[][] pianeta= new String[10][10];
		for (int i=9;i>=0;i--) {
			for (int j=0;j<10;j++) {
				pianeta[i][j]="("+(9-i)+","+(j)+")   ";
			}
		}
		pianeta[4][7]="(4,7) X ";
		pianeta[2][3]="(2,3) X ";
		assertEquals("(0,0,E)",mR.executeCommand("r"));
	}
	
	@Test
	public void testGiro_A_Sinistra() throws MarsRoverException {
		List<String>  planetObstacles  = new  ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover mR=new MarsRover(10, 10, planetObstacles);
		String[][] pianeta= new String[10][10];
		for (int i=9;i>=0;i--) {
			for (int j=0;j<10;j++) {
				pianeta[i][j]="("+(9-i)+","+(j)+")   ";
			}
		}
		pianeta[4][7]="(4,7) X ";
		pianeta[2][3]="(2,3) X ";
		assertEquals("(0,0,W)",mR.executeCommand("l"));
	}
	
	@Test
	public void testAndareAvantiNord() throws MarsRoverException {
		List<String>  planetObstacles  = new  ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover mR=new MarsRover(10, 10, planetObstacles);
		String[][] pianeta= new String[10][10];
		for (int i=9;i>=0;i--) {
			for (int j=0;j<10;j++) {
				pianeta[i][j]="("+(9-i)+","+(j)+")   ";
			}
		}
		pianeta[4][7]="(4,7) X ";
		pianeta[2][3]="(2,3) X ";
		mR.setStato("(7,6,N)");
		assertEquals("(7,6,N)",mR.getStato());
		assertEquals("(7,7,N)",mR.executeCommand("f"));
	}
	
	@Test
	public void testAndareAvantiSud() throws MarsRoverException {
		List<String>  planetObstacles  = new  ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover mR=new MarsRover(10, 10, planetObstacles);
		String[][] pianeta= new String[10][10];
		for (int i=9;i>=0;i--) {
			for (int j=0;j<10;j++) {
				pianeta[i][j]="("+(9-i)+","+(j)+")   ";
			}
		}
		pianeta[4][7]="(4,7) X ";
		pianeta[2][3]="(2,3) X ";
		mR.setStato("(7,6,S)");
		assertEquals("(7,6,S)",mR.getStato());
		System.out.println(mR.executeCommand("f"));
		assertEquals("(7,5,S)",mR.executeCommand("f"));
	}
	
	@Test
	public void testAndareAvantiEst() throws MarsRoverException {
		List<String>  planetObstacles  = new  ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover mR=new MarsRover(10, 10, planetObstacles);
		String[][] pianeta= new String[10][10];
		for (int i=9;i>=0;i--) {
			for (int j=0;j<10;j++) {
				pianeta[i][j]="("+(9-i)+","+(j)+")   ";
			}
		}
		pianeta[4][7]="(4,7) X ";
		pianeta[2][3]="(2,3) X ";
		mR.setStato("(7,6,E)");
		assertEquals("(7,6,E)",mR.getStato());
		System.out.println(mR.executeCommand("f"));
		assertEquals("(8,6,E)",mR.executeCommand("f"));
	}
	

}
