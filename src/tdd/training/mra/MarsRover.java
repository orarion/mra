package tdd.training.mra;

import java.util.List;

public class MarsRover {
	String [][]pianeta;
	String stato;
	int riga;
	int colonna;

	/**
	 * It initializes the rover at the coordinates (0,0), facing North, on a planet
	 * (represented as a grid with x and y coordinates) containing obstacles.
	 * 
	 * @param planetX         The x dimension of the planet.
	 * @param planetY         The y dimension of the planet.
	 * @param planetObstacles The obstacles on the planet. Each obstacle is a string
	 *                        (without white spaces) formatted as follows:
	 *                        "(oi_x,oi_y)". <code>null</code> if the planet does
	 *                        not contain obstacles.
	 * 
	 * @throws MarsRoverException
	 */
	public MarsRover(int planetX, int planetY, List<String> planetObstacles) throws MarsRoverException {
		pianeta=new String[planetX][planetY];
		for (int i=9;i>=0;i--) {
			for (int j=0;j<10;j++) {
				pianeta[i][j]="("+((planetX-1)-i)+","+(j)+")   ";
			}
		}
		if(planetObstacles.size()!=0) {
			for(int t=0;t<planetObstacles.size();t++) {
			String s=planetObstacles.get(t);
			int[] posizione= new int[2];
			posizione[0]=Character.getNumericValue(s.charAt(1));
			posizione[1]=Character.getNumericValue(s.charAt(3));
			pianeta[posizione[0]][posizione[1]]="("+posizione[0]+","+posizione[1]+") X ";
			}
		}
		stato="(0,0,N)";
		riga=planetX;
		colonna=planetY;
	}

	/**
	 * It returns whether, or not, the planet (where the rover moves) contains an
	 * obstacle in a cell.
	 * 
	 * @param x The x coordinate of the cell
	 * @param y The y coordinate of the cell
	 * @return <true> if the cell contains an obstacle, <false> otherwise.
	 * @throws MarsRoverException
	 */
	public boolean planetContainsObstacleAt(int x, int y) throws MarsRoverException {
		return (pianeta[x][y].contains("X"));
	}

	/**
	 * It lets the rover move on the planet according to a command string. The
	 * return string contains the new position of the rover, its direction, and the
	 * obstacles it has encountered while moving on the planet (if any).
	 * 
	 * @param commandString A string that can contain a single command -- i.e. "f"
	 *                      (forward), "b" (backward), "l" (left), or "r" (right) --
	 *                      or a combination of single commands.
	 * @return The return string that contains the position and direction of the
	 *         rover, and the obstacles the rover has encountered while moving on
	 *         the planet (if any). The return string (without white spaces) has the
	 *         following format: "(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)". x
	 *         and y define the new position of the rover while dir represents its
	 *         direction (i.e., N, S, W, or E). Finally, oi_x and oi_y are the
	 *         coordinates of the i-th encountered obstacle.
	 * @throws MarsRoverException
	 */
	public String executeCommand(String commandString) throws MarsRoverException {
		String[] puntiCardinali= {"N","E","S","W"};
		String temp=stato.substring(0,5);
		if(commandString.equals("r")) {
			turnRight(commandString,temp,puntiCardinali);
		}
		if(commandString.equals("l")) {
			turnLeft(commandString,temp,puntiCardinali);
		}
		if(commandString.equals("f")) {
			MovingForward(commandString);
		}
		return stato;
	}
	
	private void turnRight(String commandString,String temp, String[]puntiCardinali) {
		int j=0;
		for (int i=0;i<4;i++) {
			if(puntiCardinali[i].equals(stato.substring(5,6))){
				if(i==3) {
					j=0;
				}else {
					j=i+1;
				}
				stato=temp+puntiCardinali[j]+")";
				break;
			}
		}
	}
	
	private void turnLeft(String commandString,String temp, String[]puntiCardinali) {
		int j=0;
		for (int i=0;i<4;i++) {
			if(puntiCardinali[i].equals(stato.substring(5,6))){
				if(i==0) {
					j=3;
				}else {
					j=i-1;
				}
				stato=temp+puntiCardinali[j]+")";
				break;
			}
		}
	}
	
	private void MovingForward(String commandString) {
		if(stato.substring(5,6).equals("N")) {
			String temp=stato.substring(0,3);
			nord(temp,commandString, stato.substring(5,6));
		}else if (stato.substring(5,6).equals("S")){
			String temp=stato.substring(0,3);
			sud(temp,commandString, stato.substring(5,6));
		}else if (stato.substring(5,6).equals("E")) {
			String temp=stato.substring(1);
			est(temp,commandString);
		}
	}

	private void nord(String temp,String commandString, String puntoCardinale) {
		if(Integer.parseInt(stato.substring(3,4))==(colonna-1)){
			stato=temp+"0,"+puntoCardinale+")";
		}else {
			int posizione=Integer.parseInt(stato.substring(3,4))+1;
			stato=temp+posizione+","+puntoCardinale+")";
		}
	}
	
	private void sud(String temp,String commandString, String puntoCardinale) {
		if(Integer.parseInt(stato.substring(3,4))==0){
			stato=temp+(colonna-1)+","+puntoCardinale+")";
		}else {
			int posizione=Integer.parseInt(stato.substring(3,4))-1;
			stato=temp+posizione+","+puntoCardinale+")";
			
		}
	}
	
	private void est(String temp,String commandString) {
		if(Integer.parseInt(stato.substring(2,3))==riga-1){
			stato="(0,"+temp;
		}else {
			int posizione=Integer.parseInt(stato.substring(2,3))-1;
			stato="("+posizione+","+temp;
		}
	}
	
	public String getPianeta(int i, int j) {
		
		return pianeta[i][j];
	}

	public void setStato(String string) {
		stato=string;
		
	}

	public Object getStato() {
		return stato;
	}

}
